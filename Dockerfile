# - - - - Stage (0) Base build - - - - #
# Note: when updating the go minor version here, also update the go-channel in snap/snapcraft.yml
FROM golang:1.16.7-buster AS base
LABEL maintainer="Steven Allen <steven@stebalien.com>"

# Install deps
RUN apt-get update && apt-get install -y \
  libssl-dev \
  ca-certificates \
  fuse \
  bash-static  

ENV SRC_DIR /go-ipfs

# - - - - Custom  - - - - #
# Copy over our forks of libp2p to the container
COPY ./go-libp2p-kad-dht /go-libp2p-kad-dht
COPY ./go-libp2p-kbucket /go-libp2p-kbucket

# Use a seperate go.mod which points to the correct location
COPY go-ipfs/go.docker.mod 	     $SRC_DIR/go.mod
COPY go-libp2p-kad-dht/go.docker.mod /go-libp2p-kad-dht/go.mod

# Download packages first so they can be cached.
COPY go-ipfs/go.sum $SRC_DIR/
RUN cd $SRC_DIR \
  && go mod download

COPY go-ipfs/. $SRC_DIR

# Overwrite the original go.mod that was copied over in the previous command
COPY go-ipfs/go.docker.mod $SRC_DIR/go.mod

# - - - - - - - - - - - - - #

# Preload an in-tree but disabled-by-default plugin by adding it to the IPFS_PLUGINS variable
# e.g. docker build --build-arg IPFS_PLUGINS="foo bar baz"
ARG IPFS_PLUGINS

# Build the thing.
# Also: fix getting HEAD commit hash via git rev-parse.
RUN cd $SRC_DIR \
  && mkdir -p .git/objects \
  && make build GOTAGS=openssl IPFS_PLUGINS=$IPFS_PLUGINS

# Get su-exec, a very minimal tool for dropping privileges,
# and tini, a very minimal init daemon for containers
ENV SUEXEC_VERSION v0.2
ENV TINI_VERSION v0.19.0
RUN set -eux; \
    dpkgArch="$(dpkg --print-architecture)"; \
    case "${dpkgArch##*-}" in \
        "amd64" | "armhf" | "arm64") tiniArch="tini-static-$dpkgArch" ;;\
        *) echo >&2 "unsupported architecture: ${dpkgArch}"; exit 1 ;; \
    esac; \
  cd /tmp \
  && git clone https://github.com/ncopa/su-exec.git \
  && cd su-exec \
  && git checkout -q $SUEXEC_VERSION \
  && make su-exec-static \
  && cd /tmp \
  && wget -q -O tini https://github.com/krallin/tini/releases/download/$TINI_VERSION/$tiniArch \
  && chmod +x tini

# - - - - Stage (1) Build jq - - - - #
# https://github.com/wesley-dean-flexion/busybox-jq-latest
FROM alpine:3.10 AS jq
WORKDIR /workdir
RUN apk update && apk add git==2.22.5-r0 autoconf==2.69-r2 automake==1.16.1-r0 libtool==2.4.6-r6 build-base==0.5-r1
RUN git clone https://github.com/stedolan/jq.git
WORKDIR /workdir/jq
RUN git submodule update --init && autoreconf -fi && ./configure --disable-docs --disable-maintainer-mode --with-oniguruma && make -j8 LDFLAGS=-all-static && strip jq

# - - - - - Stage (2) Target image - - - - - - #
# Now comes the actual target image, which aims to be as small as possible.
FROM busybox:1.31.1-glibc
LABEL maintainer="Steven Allen <steven@stebalien.com>"

# Get the ipfs binary, entrypoint script, and TLS CAs from the build container.
ENV SRC_DIR /go-ipfs
COPY --from=base $SRC_DIR/cmd/ipfs/ipfs /usr/local/bin/ipfs
COPY --from=base $SRC_DIR/bin/container_daemon /usr/local/bin/start_ipfs
COPY --from=base /tmp/su-exec/su-exec-static /sbin/su-exec
COPY --from=base /tmp/tini /sbin/tini
COPY --from=base /bin/fusermount /usr/local/bin/fusermount
COPY --from=base /etc/ssl/certs /etc/ssl/certs

# Add suid bit on fusermount so it will run properly
RUN chmod 4755 /usr/local/bin/fusermount

# This shared lib (part of glibc) doesn't seem to be included with busybox.
COPY --from=base /lib/*-linux-gnu*/libdl.so.2 /lib/

# Copy over SSL libraries.
COPY --from=base /usr/lib/*-linux-gnu*/libssl.so* /usr/lib/
COPY --from=base /usr/lib/*-linux-gnu*/libcrypto.so* /usr/lib/

# Swarm TCP; should be exposed to the public
EXPOSE 4001
# Swarm UDP; should be exposed to the public
EXPOSE 4001/udp
# Daemon API; must not be exposed publicly but to client services under you control
EXPOSE 5001
# Web Gateway; can be exposed publicly with a proxy, e.g. as https://ipfs.example.org
EXPOSE 8080
# Swarm Websockets; must be exposed publicly when the node is listening using the websocket transport (/ipX/.../tcp/8081/ws).
EXPOSE 8081

# Create the fs-repo directory and switch to a non-privileged user.
ENV IPFS_PATH /data/ipfs
RUN mkdir -p $IPFS_PATH \
  && adduser -D -h $IPFS_PATH -u 1000 -G users ipfs \
  && chown ipfs:users $IPFS_PATH

# Create mount points for `ipfs mount` command
RUN mkdir /ipfs /ipns \
  && chown ipfs:users /ipfs /ipns

# Expose the fs-repo as a volume.
# start_ipfs initializes an fs-repo if none is mounted.
# Important this happens after the USER directive so permissions are correct.
VOLUME $IPFS_PATH

# - - - Custom - - - #
# Copy over dependencies required for `listen.sh`
COPY --from=jq /workdir/jq/jq /usr/local/bin
COPY --from=base /bin/bash-static /bin/bash
COPY ./bs58/bs58 /usr/local/bin
RUN chmod 755 /usr/local/bin/jq
RUN chmod 755 /usr/local/bin/bs58

# Copy over all of the configs and load the one
# corresponding to the specific containers hostname
COPY ./configs   /configs
COPY ./common.sh /data/ipfs/common.sh
COPY ./listen.sh /data/ipfs/listen.sh
COPY ./dump_rt.sh /data/ipfs/dump_rt.sh

# Copy over the swarm.key and overwrite the
# entrypoint with a custom version of the daemon script
# The swarm.key should be manually moved to /data/ipfs/
# to use a private swarm
COPY ./swarm.key /swarm.key
COPY ./util/start_ipfs.sh /usr/local/bin/start_ipfs

RUN chown -R ipfs:users /configs
RUN chown ipfs:users /swarm.key
RUN chmod 666 /swarm.key

# - - - - - - - - - - #

# Fix permissions on start_ipfs (ignore the build machine's permissions)
RUN chmod 0755 /usr/local/bin/start_ipfs

# The default logging level
ENV IPFS_LOGGING ""

# This just makes sure that:
# 1. There's an fs-repo, and initializes one if there isn't.
# 2. The API and Gateway are accessible from outside the container.
ENTRYPOINT ["/sbin/tini", "--", "/usr/local/bin/start_ipfs"]

# Heathcheck for the container
# QmUNLLsPACCz1vLxQVkXqqLX5R1X345qqfHbsf67hvA3Nn is the CID of empty folder
#HEALTHCHECK --interval=30s --timeout=3s --start-period=5s --retries=3 \
#  CMD ipfs dag stat /ipfs/QmUNLLsPACCz1vLxQVkXqqLX5R1X345qqfHbsf67hvA3Nn || exit 1 
